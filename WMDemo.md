Demonstrate Working Markdown
============================

Intro
-----
This is a markdown file. It contains some WorkingWiki tags.

Install
-------
The instructions to install Working Markdown for local use and on a Jekyll
server are
[here](http://lalashan.mcmaster.ca/theobio/projects/index.php/Working_Markup).
This will also tell you how to install the Dushoff lab R rules, if you want them
(you do).

To use the Working Markdown engine, in addition to those instructions, you will
definitely need to install:

* markdown

Additionally, if you want to use the Dushoff lab rules you will need:

* R
* pdflatex
* pdftk
* pdf2svg
* maybe other things too. I don't know, I'm not Wonder Woman.

Demo
----

An R script appears.

<source-file filename="Ascript.R">
x = c(1,2,3,4,5)
y = c(0,2,1,2,0)

plot(x,y, type="b")
</source-file>

Run R script.

<project-file filename="Ascript.Rout"/>

Look at pictures.

<project-file filename="Ascript.Rout-0.svg"/>

More stuff
----------

As with WW, you can make your own `make` rules and files for use with
your source files. Do whatever you want.

Making it Go
------------

It's a multi-step process. We've coded the steps into a makefile for
convenience, so we can do all the processing from .md file
to .html by typing on the command line,<sup>\*</sup>

    $ make WMDemo.html

Discuss!

<hr/>
<sub><sup>*</sup>(or `$ make WW_DIR=/some/location WMDemo.html`, if you have
the WorkingWiki/WMD code installed in a different place.)</sub>
